﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

	public float levelSeconds = 100;
	private GameObject winLabel;
	private Slider slider;
	private AudioSource audioSource;
	public LevelManager levelManager;
	private bool isEndOfLevel = false;

	// Use this for initialization
	void Start () {
		slider = GetComponent<Slider> ();
		audioSource = GetComponent<AudioSource> ();
		FindYouWinLabel ();
	}

	void FindYouWinLabel ()
	{
		winLabel = GameObject.Find ("You Win");
		winLabel.SetActive (false);
		if (!winLabel) {
			Debug.LogError ("Not Found You Win label!");
		}
	}
	
	// Update is called once per frame
	void Update () {
		slider.value = Time.timeSinceLevelLoad / levelSeconds;
		bool timeIsUp = (Time.timeSinceLevelLoad >= levelSeconds);
		if (timeIsUp && !isEndOfLevel) {
			HandleWinCodition ();
		}
	}

	void HandleWinCodition ()
	{
		DestroyOnTarggedObjects ();
		audioSource.Play ();
		winLabel.SetActive (true);
		Invoke ("LoadNextMission", audioSource.clip.length);
		isEndOfLevel = true;
	}

	// Destroy on object when win tag = "DestroyOnWin"
	void DestroyOnTarggedObjects(){
		GameObject[] targgedObjects = GameObject.FindGameObjectsWithTag ("DestroyOnWin");
		foreach (GameObject targgedObject in targgedObjects) {
			DestroyObject (targgedObject);
		}
	}

	void LoadNextMission() {
		levelManager.LoadNextLevel();
	}
}
