﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public AudioClip[] levelMusicChangeArray;

    private AudioSource audioSource;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
		float savedVolume = PlayerPrefsManager.GetMasterVolume ();
		audioSource.volume = savedVolume;
		audioSource.clip = levelMusicChangeArray [0];
		audioSource.Play ();
	}

    private void OnLevelWasLoaded(int level)
    {
        AudioClip thisLevelMusic = levelMusicChangeArray[level];
        if (thisLevelMusic) {
            audioSource.clip = thisLevelMusic;
            audioSource.loop = true;
            audioSource.Play();
        }
    }

	public void SetVolume(float volume) {
		audioSource.volume = volume;
	}

    // Update is called once per frame
    void Update () {
		
	}
}
