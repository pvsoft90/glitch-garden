﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

	public GameObject projectile, gun;

    private GameObject projectileParent;
	private Animator animator;

	private Spawner myLaneSpawner;

    private void Start()
    {
		animator = gameObject.GetComponent<Animator> ();
//		animator = GameObject.FindObjectOfType<Animator> ();
        projectileParent = GameObject.Find("Projectiles");
        if (!projectileParent) {
            projectileParent = new GameObject();
            projectileParent.name = "Projectiles";
        }
		SetMyLaneSpawner ();
		print (myLaneSpawner);
    }

	void Update() {
		if (IsAttackerAheadLane()) {
			animator.SetBool ("isAttacking", true);
		} else {
			animator.SetBool ("isAttacking", false);
		}
	}

	void SetMyLaneSpawner() {
		Spawner[] spawnerArray = GameObject.FindObjectsOfType<Spawner> ();
		foreach (Spawner spawner in spawnerArray) {
			if (spawner.transform.position.y == gameObject.transform.position.y) {
				myLaneSpawner = spawner;
				return;
			}
		}
		Debug.LogError ("Can not find Lane Spawner!");
	}

	bool IsAttackerAheadLane() {

		//Exit if no attackers in lane
		if (myLaneSpawner.transform.childCount <= 0) {
			return false;
		}

		// If there are attackers, are they ahea?
		foreach (Transform child in myLaneSpawner.transform) {
			if (child.transform.position.x > transform.position.x) {
				return true;
			}
		}

		//Attackers in lane, but behide us.
		return true;
	}

    private void Fire() {
		GameObject newProjectile = Instantiate (projectile) as GameObject;
		newProjectile.transform.parent = projectileParent.transform;
		newProjectile.transform.position = gun.transform.position;
	}
}
