﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class Attacker : MonoBehaviour {

//	[Range (-1f, 1.5f)]
	private float currentSpeed;
	private GameObject currentTarget;
	private Animator animator;
    [Tooltip("Average number of seconds between appearances")]
    public float seenEverySeconds;

	// Use this for initialization
	void Start () {
		animator = this.GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left * currentSpeed * Time.deltaTime);
		if (currentTarget == null) {
			animator.GetComponent<Animator> ();
			animator.SetBool ("isAttacking", false);
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Debug.Log (name + " trigger enter - attacker");
	}

	public void SetSpeed(float speed) {
		currentSpeed = speed;
	}

	// Called from the animator at time of actual blow
	public void StrikeCurrentTarget(float damage){
		if (currentTarget) {
			Health health = currentTarget.GetComponent<Health> ();
			if (health) {
				health.DealDamage (damage);
			}
		}
	}

	public void Attack(GameObject targetObj) {
		currentTarget = targetObj;
	}

}
