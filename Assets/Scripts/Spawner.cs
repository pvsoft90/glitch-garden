﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] attackerPrefabArray;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        foreach (GameObject thisAttacker in attackerPrefabArray) {
            if(IsTimeToSpawn(thisAttacker))
            {
                Spawn(thisAttacker);
            }
        }	
	}

    void Spawn(GameObject myGameObject) {
        GameObject myAttacker = Instantiate(myGameObject);
        myAttacker.transform.parent = transform;
        myAttacker.transform.position = transform.position;
    }

    bool IsTimeToSpawn(GameObject  attackerGameObject) {
        Attacker attacker = attackerGameObject.GetComponent<Attacker>();
        float meanSpawnDelay = attacker.seenEverySeconds;
        float spawnPerSecond = 1 / meanSpawnDelay ; // lay 1 giay chia giay muon xuat hien

        if (Time.deltaTime > meanSpawnDelay) {
            Debug.Log("Spawn rate capped by frame rate!");
        }

        float threshold = spawnPerSecond * Time.deltaTime / 5; // 5 lane

		return (Random.value < threshold);
    }


}
