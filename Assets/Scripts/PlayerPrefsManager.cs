﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour {

	const string MASTER_VOLUME = "master_volume";
	const string DIFFICULTY_KEY = "difficulty_key";
	const string LEVEL_KEY = "level_unlocked";

	public static void SetMasterVolume(float volume) {
		if (volume >= 0f && volume <= 1f) {
			PlayerPrefs.SetFloat (MASTER_VOLUME, volume);
		} else {
			Debug.LogError ("Master voulume out of range!");
		}
	}

	public static float GetMasterVolume() {
		return PlayerPrefs.GetFloat (MASTER_VOLUME);
	}

	public static void UnlockLevel(int level) {
		if (level <= SceneManager.GetActiveScene ().buildIndex - 1) {
			PlayerPrefs.SetInt (LEVEL_KEY + level.ToString(), 1); //use 1 for true
		} else {
			Debug.LogError ("Trying to unclock level not in build order!");
		}
	}

	public static bool IsLevelUnlocked(int level) {
		if (level <= SceneManager.GetActiveScene ().buildIndex - 1) {
			if (PlayerPrefs.GetFloat (LEVEL_KEY + level.ToString()) == 1f) {
				return true;
			} else {
				return false;
			}
		} else {
			Debug.LogError ("Trying to unclock level not in build order!");
			return false;
		}
	}

	public static void SetDifficulty(float difficult) {
		if (difficult >= 1 && difficult <= 3) {
			PlayerPrefs.SetFloat (DIFFICULTY_KEY, difficult);
		} else {
			Debug.LogError("Volume of Difficulty was out of range (hint: 0 < volume < 1)!");
		}

	}

	public static float GetDifficulty() {
		return PlayerPrefs.GetFloat (DIFFICULTY_KEY);
	}

}
